<?php
/*
Plugin Name: WPezToolbox - Bundle: Default (POC)
Plugin URI: https://gitlab.com/wpezsuite/wpeztoolbox/wpez-toolbox-bundle-default
Description: The default bundle for WPezToolbox - Loader
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxBundleDefault;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__. '\plugins');


function plugins( $arr_in = [] ) {

	$str_bundle           = __( 'DFLT', 'wpez_tbox' );
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = [

		'wpez-toolbox-bundle-anti-spam' =>
			[
				'name'     => $str_prefix . 'WPezToolbox - Bundle: Anti-spam',
				'slug'     => 'wpez-toolbox-bundle-anti-spam',
				'source'   => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/16154895/wpeztoolbox.png?width=64',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Mark "Chief Alchemist" Simchock for Alchemy United',
						'url_by'  => 'https://AlchemyUnited.com',
						'desc'    => 'ANTSPM - A WPezToolbox bundle of WordPress anti-spam plugins.',
						'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/16154895/wpeztoolbox.png?width=64',
					],

					'resources' => [
						'url_repo' => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-anti-spam',
						'url_site' => 'https://gitlab.com/wpezsuite/WPezToolbox',
						'url_tw'   => 'https://twitter.com/WPezDeveloper'
					]
				]
			],
		
		'wpez-toolbox-bundle-cache' =>
			[
				'name'     => $str_prefix . 'WPezToolbox - Bundle: Cache',
				'slug'     => 'wpez-toolbox-bundle-cache',
				'source'   => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev/-/archive/master/wpez-toolbox-bundle-cache-master.zip',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Mark "Chief Alchemist" Simchock for Alchemy United',
						'url_by'  => 'https://AlchemyUnited.com',
						'desc'    => 'CCH - A WPezToolbox bundle of WordPress cache plugins.',
						'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/15402362/wpeztoolbox.png?width=64',
					],

					'resources' => [
						'url_repo' => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-cache',
						'url_site' => 'https://gitlab.com/wpezsuite/WPezToolbox',
						'url_tw'   => 'https://twitter.com/WPezDeveloper'
					]
				]
			],

		'wpez-toolbox-bundle-dev' =>
			[
				'name'     => $str_prefix . 'WPezToolbox - Bundle: Dev',
				'slug'     => 'wpez-toolbox-bundle-dev',
				'source'   => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev/-/archive/master/wpez-toolbox-bundle-dev-master.zip',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Mark "Chief Alchemist" Simchock for Alchemy United',
						'url_by'  => 'https://AlchemyUnited.com',
						'desc'    => 'DEV - A bundle of developer-centic WordPress plugin tools.',
						'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/15292865/wpeztoolbox.png?width=64',
					],

					'resources' => [
						'url_repo' => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev',
						'url_site' => 'https://gitlab.com/wpezsuite/WPezToolbox',
						'url_tw'   => 'https://twitter.com/WPezDeveloper'
					]
				]
			],

		'wpez-toolbox-bundle-seo' =>
			[
				'name'     => $str_prefix . 'WPezToolbox - Bundle: SEO',
				'slug'     => 'wpez-toolbox-bundle-seo',
				'source'   => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-seo/-/archive/master/wpez-toolbox-bundle-seo-master.zip',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Mark "Chief Alchemist" Simchock for Alchemy United',
						'url_by'  => 'https://AlchemyUnited.com',
						'desc'    => 'SEO - A WPezToolbox bundle of WordPress SEO plugins.',
						'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/15440401/wpeztoolbox.png?width=64',
					],

					'resources' => [
						'url_repo' => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-seo',
						'url_site' => 'https://gitlab.com/wpezsuite/WPezToolbox',
						'url_tw'   => 'https://twitter.com/WPezDeveloper'
					]
				]
			],

	];

	$arr_mod = apply_filters( __NAMESPACE__ . '\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}